import unittest
from appium import webdriver


class Android_ATP_WTA(unittest.TestCase):
    "Class to run tests against the ATP WTA app"

    def setUp(self):
        "Setup for the test"
        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['platformVersion'] = '5.1'
        desired_caps['deviceName'] = '5T75BEJJ755SS4GQ'
        desired_caps['appPackage'] = 'mobisocial.arcade'
        desired_caps['appActivity'] = '.sdk.activity.ArcadeSignInActivity'
        desired_caps['noReset'] = 'true'

        self.driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)

    def tearDown(self):
        "Tear down the test"
        self.driver.quit()

    def test_log_in_successfully(self):
        self.driver.implicitly_wait(30)

        login_btn = self.driver.find_element_by_id('mobisocial.arcade:id/have_account')
        login_btn.click()

        omlet_id = self.driver.find_element_by_id('omid')
        omlet_id.click()
        omlet_id.send_keys('apple.y')

        omlet_pass = self.driver.find_element_by_id('pass')
        omlet_pass.click()
        omlet_pass.send_keys('omlet5566')

        go_btn = self.driver.find_element_by_class_name('android.widget.Button')
        go_btn.click()

        search_bar = self.driver.find_element_by_id('mobisocial.arcade:id/search_bar_wrapper')
        search_bar.click()

        # search_layout = self.driver.find_element_by_id('mobisocial.arcade:id/search_layout')
        # search_layout.click()

        search_bar_on_dicovery_page = self.driver.find_element_by_id('mobisocial.arcade:id/search_view')
        search_bar_on_dicovery_page.click()
        search_bar_on_dicovery_page.send_keys("apple.y")

        name = self.driver.find_element_by_id('mobisocial.arcade:id/text_view_profile_about')
        name.click()
